package com.example.frndproject

import okhttp3.MultipartBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part

interface AudioService {
    @Multipart
    @POST("/api/uploadMedia")
    suspend fun uploadAudioFile(
        @Part file1: MultipartBody.Part
    )
}