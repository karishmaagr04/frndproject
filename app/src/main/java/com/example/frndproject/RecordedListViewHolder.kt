package com.example.frndproject

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.media_item.view.*
import java.io.File

class RecordedListViewHolder(onClickListener: OnButtonClickListener, itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    private var onClickListener: OnButtonClickListener? = null

    init {
        this.onClickListener = onClickListener
    }

    fun bindData(filemap: File, state: Int?) {
        with(itemView) {
            if (state == 0) {
                playButton.setImageDrawable(resources.getDrawable(R.drawable.ic_play_arrow))
            } else {
                playButton.setImageDrawable(resources.getDrawable(R.drawable.ic_pause))
            }
            musicName.text = filemap.name

            itemView.setOnClickListener {

                if (state == 0) {
                    onClickListener?.onPlayButtonClick(position = adapterPosition)
                } else {
                    onClickListener?.onPauseButtonClick(adapterPosition)
                }
            }
        }
    }

}