package com.example.frndproject


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Optional

@Serializable
data class Data(
    @Optional
    @SerialName("id")
    val id: Int? = null,
    @Optional
    @SerialName("media")
    val media: String? = null,
    @Optional
    @SerialName("user_id")
    val userId: Int? = null
)