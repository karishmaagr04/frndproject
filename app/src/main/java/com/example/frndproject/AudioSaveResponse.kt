package com.example.frndproject


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class AudioSaveResponse(

    @SerialName("data")
    val `data`: Data? = null,

    @SerialName("message")
    val message: String? = null,

    @SerialName("status")
    val status: Int? = null
)