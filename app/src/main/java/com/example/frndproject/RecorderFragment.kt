package com.example.frndproject

import android.Manifest
import android.content.pm.PackageManager
import android.media.MediaRecorder
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_recording.*
import kotlinx.io.IOException
import java.io.File


private const val LOG_TAG = "RecorderFragment"
private const val REQUEST_RECORD_AUDIO_PERMISSION = 200

class RecorderFragment : Fragment() {

    private var audioViewModel: AudioViewModel? = null
    private var fileName: String = ""
    private var fileUri: Uri? = null
    private var mediaRecorder: MediaRecorder? = null

    // Requesting permission to RECORD_AUDIO
    private var permissionToRecordAccepted = false
    private var permissions: Array<String> = arrayOf(Manifest.permission.RECORD_AUDIO)

    private val playIcon by lazy {
        ContextCompat.getDrawable(
            context!!,
            R.drawable.ic_play_arrow
        )
    }
    private val pauseIcon by lazy {
        ContextCompat.getDrawable(
            context!!,
            R.drawable.ic_pause
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_recording, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        audioViewModel = ViewModelProviders.of(this).get(AudioViewModel::class.java)

        // Record to the external cache directory for visibility
        ActivityCompat.requestPermissions(activity!!, permissions, REQUEST_RECORD_AUDIO_PERMISSION)

        setListeners()

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionToRecordAccepted = if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        } else {
            false
        }
    }


    private fun setListeners() {
        recordButton.setOnClickListener {
            if (recordButton.isChecked) {
                startRecording()
            } else {
                stopRecording()
                fileUri = Uri.fromFile(File(fileName))
                audioViewModel?.uploadAudioFile(fileUri!!)
            }
        }
    }


    private fun startRecording() {
        val fileUniqueName: String = System.currentTimeMillis().toString() + "audio.3gp"
        fileName =
            "${context?.externalCacheDir?.absolutePath}/${fileUniqueName}"
        mediaRecorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setOutputFile(fileName)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)

            try {
                prepare()
                start()
            } catch (e: IOException) {
                Log.e(LOG_TAG, "prepare() failed")
            }
        }
    }

    private fun stopRecording() {
        mediaRecorder?.apply {
            stop()
            release()
        }
        mediaRecorder = null
    }
}
