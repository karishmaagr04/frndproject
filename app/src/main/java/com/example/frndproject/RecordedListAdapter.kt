package com.example.frndproject

import android.media.MediaPlayer
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import java.io.File
import java.io.IOException

class RecordedListAdapter(private var file: Array<File>) :
    RecyclerView.Adapter<RecordedListViewHolder>(), OnButtonClickListener {
    private var player: MediaPlayer? = null
    private var hashMap: HashMap<File, Int> = HashMap()

    init {
        for (i in 0..file.size - 1) {
            hashMap.put(file[i], 0)
        }
    }

    override fun onPlayButtonClick(position: Int) {
        for (i in 0..hashMap.size-1) {
            if (i == position) {
                hashMap.set(file[i], 1)
            } else {
                hashMap.set(file[i], 0)
            }
        }
        startPlaying(file[position])
        notifyDataSetChanged()
    }

    override fun onPauseButtonClick(position: Int) {
        hashMap.set(file[position], 0)
        stopPlaying()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecordedListViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.media_item, parent, false)
        return RecordedListViewHolder(this, view)
    }

    override fun getItemCount(): Int {
        return file.size
    }

    fun updateFileList(fileList: Array<File>) {
        file = emptyArray()
        this.file = fileList
        for (i in 0..file.size - 1) {
            hashMap.put(file[i], 0)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecordedListViewHolder, position: Int) {
        holder.bindData(file[position], hashMap[file[position]])
    }

    private fun startPlaying(file: File) {
        player = MediaPlayer().apply {
            try {
                setDataSource(file.name)
                prepare()
                start()
            } catch (e: IOException) {
                Log.e("Exception", "prepare() failed")
            }
        }
    }

    private fun stopPlaying() {
        player?.release()
        player = null
    }

}