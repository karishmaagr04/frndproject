package com.example.frndproject

import android.net.Uri
import androidx.core.net.toFile
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody

class AudioViewModel : ViewModel() {

    private val bookingData by lazy {
        MutableLiveData<String>()
    }

    fun uploadAudioFile(audioUrl : Uri) {
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = withContext(Dispatchers.IO) {
                    prepareFilePart(audioUrl)?.let {
                        RetrofitClientInstance.retrofit.create(AudioService::class.java)
                            .uploadAudioFile(it)
                    }
                }
            } catch (e: Exception) {
                // we can catch exception here
                e.printStackTrace()
            }
        }
    }

    private fun prepareFilePart(fileUri: Uri?): MultipartBody.Part? {
        // use the FileUtils to get the actual file by uri
        val file = fileUri?.toFile()
        val requestFile = file?.asRequestBody("*/*".toMediaTypeOrNull())

        // MultipartBody.Part is used to send also the actual file name
        requestFile?.let {
            return MultipartBody.Part.createFormData("media", file.name, requestFile)
        } ?: kotlin.run {
            return null
        }
    }
}