package com.example.frndproject

import java.io.File
import java.text.FieldPosition

interface OnButtonClickListener {
    abstract  fun onPlayButtonClick(position : Int)
    abstract fun onPauseButtonClick(position : Int)
}