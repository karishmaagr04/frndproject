package com.example.frndproject

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

object RetrofitClientInstance {

    val retrofit: Retrofit by lazy {
        val httpClient = OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
            httpClient.addInterceptor(logging)
        }
        httpClient
            .readTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
        return@lazy Retrofit.Builder()
            .baseUrl("http://34.93.132.144:8080")
            .addConverterFactory(
                Json(JsonConfiguration.Stable.copy(strictMode = false))
                    .asConverterFactory("application/json".toMediaType())
            )
            .client(httpClient.build())
            .build()
    }
}