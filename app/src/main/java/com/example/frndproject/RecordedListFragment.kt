package com.example.frndproject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_recorded_list.*
import java.io.File

class RecordedListFragment : Fragment() {
    private var file: Array<File>? = null
    private var recordedListAdapter: RecordedListAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_recorded_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        file = (listOf(context?.externalCacheDir?.listFiles())[0])
        file?.let {
            recordedListAdapter = RecordedListAdapter(it)
        }
        recyclerView.adapter = recordedListAdapter

    }

    override fun onResume() {
        super.onResume()
        file = (listOf(context?.externalCacheDir?.listFiles())[0])
        file?.let { recordedListAdapter?.updateFileList(it) }
    }

}