package com.example.frndproject

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class AudioRecorderPagerAdapter(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val titles by lazy { ArrayList<String>() }
    private val fragments by lazy {
        ArrayList<Fragment>()
    }

    init {
        titles.add("Recorder")
        titles.add("Recorded List")
        fragments.add(RecorderFragment())
        fragments.add(RecordedListFragment())
    }

    override fun getCount(): Int {
        return titles.size
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }


}